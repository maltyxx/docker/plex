# Plex Docker Multi-Architecture Project

## About the Project
This project focuses on building Docker images for Plex Media Server (PMS) suitable for various architectures, including amd64 and arm64. This is particularly useful for IoT systems based on devices like Orange Pi. It leverages GitLab's CI/CD capabilities to automate the building and deployment processes.

## Features
- **Multi-Architecture Building:** Creates Docker images for amd64 and arm64 architectures.
- **GitLab CI/CD Integration:** Uses GitLab CI/CD pipelines for automated building and deployment.
- **Automated Updates:** Incorporates the latest updates from Plex via a Git submodule.

## Prerequisites
- Git
- Docker & Docker Buildx
- A GitLab account (for using CI/CD)

## Installation
1. Clone the repository:
   ```bash
   git clone https://gitlab.com/maltyxx/docker/plex
   cd docker/plex
   ```

2. Initialize and update the submodules:
   ```bash
   git submodule init
   git submodule update
   ```

## Usage
- **Triggering Builds:** Builds are triggered via the GitLab CI/CD pipelines configured in the `.gitlab-ci.yml` file.

- **Updating the Submodule:**
  To update the `pms-docker` submodule with the latest changes:
   ```bash
   cd pms-docker
   git pull origin master
   cd ..
   git commit -am "Update pms-docker submodule"
   git push
   ```

## Contributing
Contributions are always welcome. Feel free to fork the project, create a feature branch, and submit a pull request.

## License
[MIT](LICENSE)
